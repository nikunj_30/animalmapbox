
import {NotificationsPage} from "../notifications/notifications";
import {SettingsPage} from "../settings/settings";
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController,PopoverController,ModalController } from 'ionic-angular';
import {ApiService} from '../../services/api.service';
import { HostPopupPage } from '../host-popup/host-popup';

// import { ApiService } from "../../_services/api.service";
// import { MessagesPage } from '../messages/messages';
// import { ProfilePage } from '../profile/profile';
// import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  tender: any;
  proposals_array: any = []
  marker: any;

  pointer_array:any = [
  {
    lat:32.7940463,
    lng:34.98957100000007
  },
  {
    lat:32.832154,
    lng:35.08840699999996
  }
]
  constructor(     public modalCtrl: ModalController,
    public api:ApiService, public popoverCtrl: PopoverController,public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams) {

    this.tender = this.navParams.get('tender');
   this.insert_codelab_test();
  }


  openhost(){

    let profileModal = this.modalCtrl.create(HostPopupPage);
   profileModal.present();



  }
  insert_codelab_test(){
                try {
                  let obj:any ={}
                this.api.insert_codelab_test('insert_codelab_test',obj).then((data:any)=>{
                if(data == 0){
                  console.log('server error');
                }else{
                  console.log('data: ', data);
                }})     
                } catch (err) {
                console.log(err);
                }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
   
  }

  goToAccount() {
    this.navCtrl.push(SettingsPage);
  }

  presentNotifications(myEvent) {
    console.log(myEvent);
    let popover = this.popoverCtrl.create(NotificationsPage);
    popover.present({
      ev: myEvent
    });
  }


  public getProfileImage(freelancer) {
    let profile = freelancer.profile_photo;
    let facebookId = freelancer.facebook_id;
    // let zoomaurl = this.settings.ImageUrl + "" + freelancer.id + "/" + freelancer.profile_photo;
    if (profile == null && facebookId == null) {
      return "assets/img/avatar4.jpg";
    }
    if (facebookId != null) {
      return `https://graph.facebook.com/${facebookId}/picture?width=9999`;
    }
    if (profile != null) {
      return "http://dev.zooma-app.com/images/" + freelancer.profile_photo;
    }
  }

 


  public hotels = [
    {
      location: {
        lat: -22.906847,
        lon: -43.172896,
      }
    },
    {
      location: {
        lat: -22.969778,
        lon: -43.186859,
      },

    }
  ]

  public agmStyles = [
    // {
    //   "elementType": "geometry",
    //   "stylers": [
    //     {
    //       "color": "#1d2c4d"
    //     }
    //   ]
    // },
    // {
    //   "elementType": "labels.text.fill",
    //   "stylers": [
    //     {
    //       "color": "#8ec3b9"
    //     }
    //   ]
    // },
    // {
    //   "elementType": "labels.text.stroke",
    //   "stylers": [
    //     {
    //       "color": "#1a3646"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "administrative.country",
    //   "elementType": "geometry.stroke",
    //   "stylers": [
    //     {
    //       "color": "#4b6878"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "administrative.land_parcel",
    //   "elementType": "labels.text.fill",
    //   "stylers": [
    //     {
    //       "color": "#64779e"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "administrative.province",
    //   "elementType": "geometry.stroke",
    //   "stylers": [
    //     {
    //       "color": "#4b6878"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "landscape.man_made",
    //   "elementType": "geometry.stroke",
    //   "stylers": [
    //     {
    //       "color": "#334e87"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "landscape.natural",
    //   "elementType": "geometry",
    //   "stylers": [
    //     {
    //       "color": "#023e58"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "poi",
    //   "elementType": "geometry",
    //   "stylers": [
    //     {
    //       "color": "#283d6a"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "poi",
    //   "elementType": "labels.text.fill",
    //   "stylers": [
    //     {
    //       "color": "#6f9ba5"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "poi",
    //   "elementType": "labels.text.stroke",
    //   "stylers": [
    //     {
    //       "color": "#1d2c4d"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "poi.park",
    //   "elementType": "geometry.fill",
    //   "stylers": [
    //     {
    //       "color": "#023e58"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "poi.park",
    //   "elementType": "labels.text.fill",
    //   "stylers": [
    //     {
    //       "color": "#3C7680"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "road",
    //   "elementType": "geometry",
    //   "stylers": [
    //     {
    //       "color": "#304a7d"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "road",
    //   "elementType": "labels.text.fill",
    //   "stylers": [
    //     {
    //       "color": "#98a5be"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "road",
    //   "elementType": "labels.text.stroke",
    //   "stylers": [
    //     {
    //       "color": "#1d2c4d"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "road.arterial",
    //   "stylers": [
    //     {
    //       "visibility": "off"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "road.highway",
    //   "elementType": "geometry",
    //   "stylers": [
    //     {
    //       "color": "#2c6675"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "road.highway",
    //   "elementType": "geometry.stroke",
    //   "stylers": [
    //     {
    //       "color": "#255763"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "road.highway",
    //   "elementType": "labels",
    //   "stylers": [
    //     {
    //       "visibility": "off"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "road.highway",
    //   "elementType": "labels.text.fill",
    //   "stylers": [
    //     {
    //       "color": "#b0d5ce"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "road.highway",
    //   "elementType": "labels.text.stroke",
    //   "stylers": [
    //     {
    //       "color": "#023e58"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "road.local",
    //   "stylers": [
    //     {
    //       "visibility": "off"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "transit",
    //   "elementType": "labels.text.fill",
    //   "stylers": [
    //     {
    //       "color": "#98a5be"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "transit",
    //   "elementType": "labels.text.stroke",
    //   "stylers": [
    //     {
    //       "color": "#1d2c4d"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "transit.line",
    //   "elementType": "geometry.fill",
    //   "stylers": [
    //     {
    //       "color": "#283d6a"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "transit.station",
    //   "elementType": "geometry",
    //   "stylers": [
    //     {
    //       "color": "#3a4762"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "water",
    //   "elementType": "geometry",
    //   "stylers": [
    //     {
    //       "color": "#0e1626"
    //     }
    //   ]
    // },
    // {
    //   "featureType": "water",
    //   "elementType": "labels.text.fill",
    //   "stylers": [
    //     {
    //       "color": "#4e6d70"
    //     }
    //   ]
    // }
  ]
}


