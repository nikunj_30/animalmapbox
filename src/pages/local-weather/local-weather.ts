import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
// import { maptoken } from './../../../www/assets/maptoken';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'page-local-weather',
  templateUrl: 'local-weather.html'
})
export class LocalWeatherPage {



  accessToken = 'pk.eyJ1IjoiZWRlbmdvZXRhIiwiYSI6ImNqdHdzaTdwYjAwdjA0NG1oNDd4cjZuYnYifQ.dA7XfSR5-xK2kM59yXsHfQ';



  // style: 'mapbox://styles/mapbox/streets-v11'

  map: mapboxgl.Map;
  style = 'mapbox://styles/mapbox/outdoors-v9';
  lat = 32.794044;
  lng = 34.989571;

  constructor(public navCtrl: NavController) {
    mapboxgl.accessToken = this.accessToken;
  }

  ionViewDidLoad(){
this.buildMap();
console.log(mapboxgl.accessToken);
  }

  buildMap() {
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: 13,
      center: [this.lng, this.lat]
    });

console.log(this.map);
    }
  }



