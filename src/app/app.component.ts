import { Component, ViewChild, OnInit } from "@angular/core";
import { Platform, Nav } from "ionic-angular";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { LocalWeatherPage } from "../pages/local-weather/local-weather";
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import {ApiService} from '../services/api.service';

export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp implements OnInit{
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  appMenuItems: Array<MenuItem>;

  constructor(
    public api:ApiService,
    private push:Push,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public keyboard: Keyboard
  ) {
    this.initializeApp();

    this.appMenuItems = [
      {title: 'Home', component: HomePage, icon: 'home'},
      {title: 'Local Weather', component: LocalWeatherPage, icon: 'partly-sunny'}
    ];

  }




ngOnInit(){
 

}


  pushSetup(){
    const options: PushOptions = {
      android: {
        senderID:'674215514607'
      },
      ios: {
          alert: 'true',
          badge: true,
          sound: 'false'
      }
   }
   
   const pushObject: PushObject = this.push.init(options);
   
   
   pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));
   
   pushObject.on('registration').subscribe((registration: any) => {console.log('Device registered', registration.registrationId)
  // let deviceToken =  registration.registrationId;

// let obj:any ={
//   'push_token': registration.registrationId
// }
//   this.api.register_push('register_push',obj).then(data=>{
//     console.log(data)
//   })
  }
   );
   
   pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));



  }






  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.

      //*** Control Splash Screen
      // this.splashScreen.show();
      // this.splashScreen.hide();

      //*** Control Status Bar
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);

      //*** Control Keyboard
      // this.pushSetup();

      this.push.hasPermission()
                .then((res: any) => {

                    if (res.isEnabled) {
                        console.log('We have permission to send push notifications');
                    } else {
                        console.log('We do not have permission to send push notifications');
                    }

                });
            const options: PushOptions = {
                android: {
                    senderID: "674215514607",
                    'iconColor': '#000000',
                    "forceShow": true
                },
                ios: {
                    alert: 'true',
                    badge: true,
                    sound: 'false'
                }
            };
            const pushObject: PushObject = this.push.init(options);
            pushObject.on('notification').subscribe((notification: any) => {
                console.log('Received a notification', notification)
            });

            pushObject.on('registration').subscribe((registration: any) => {
                console.log('Device registered', registration);
                window.localStorage.setItem("Devicetoken", registration.registrationId);
            });
            pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
    
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    this.nav.setRoot(LoginPage);
  }

}
