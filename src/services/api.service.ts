
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable, EventEmitter, Output} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map'
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import { Subject } from 'rxjs/Subject';
import {HttpHeaders} from "@angular/common/http";

 
// import { cursorTo } from "readline";



//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
@Injectable()

export class ApiService
{


    private _listners = new Subject<any>();

    listen(): Observable<any> {
       return this._listners.asObservable();
    }

    filter(filterBy: string) {
       this._listners.next(filterBy);
    }









    @Output() onFilter = new EventEmitter();
    public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    public options = new RequestOptions({headers:this.headers});
    
    public Items:any[]=[];
    public Branches:any[]=[];
    public Professions:any[]=[];
    
   
    
    private _groups: BehaviorSubject<Array<any>> = new BehaviorSubject(null);
    readonly groups$: Observable<Array<any>> = this._groups.asObservable();
    get groups():Array<any>{return this._groups.getValue();}
  
    public headers2 = new Headers();
   
    
    // public headers1 = new HttpHeaders(pu
    //     {
    //         'Authorization' : "Basic YXBpOmFwSTEwNTY=",
    //         'content-Type'  : 'application/json',
    //         'Accept'        : 'application/json',
    //         'id'        : localStorage.id,
    //         'token'        : localStorage.token
    //     }
    // )
    //
    // public Options = { headers: this.headers1 };
    

    public ServerUrl = "http://www.codelabltd.com/api/";

    constructor(private http:Http)
    {
       
    
        this.headers2.append('Content-Type','application/json');
        this.headers2.append('Access-Control-Allow-Origin','*');
       
    
    };
    
    
   
    async insert_codelab_test(url:string,obj:Object): Promise<Array<any>> {
        return new Promise<Array<any>>(async (resolve, reject) => {
            let body = new FormData();
            // body.append('todo_id', id);
            // body.append('event_id', event_id );
            // body.append('freelancer_id',id);
            body.append('obj', JSON.stringify(obj));
           
          
            try {
                let answer = await this.http.post(this.ServerUrl + '' + url,body).map(res => res.json()).do((data)=>{}).toPromise();
                resolve(answer);
            } catch (err) {
                reject(err);
            } finally {
                //loading.dismiss();
            }
        });
    }


    async register_push(url:string,obj:Object): Promise<Array<any>> {
        return new Promise<Array<any>>(async (resolve, reject) => {
            let body = new FormData();
            // body.append('todo_id', id);
            // body.append('event_id', event_id );
            // body.append('freelancer_id',id);
            body.append('obj', JSON.stringify(obj));
           
          
            try {
                let answer = await this.http.post(this.ServerUrl + '' + url,body).map(res => res.json()).do((data)=>{}).toPromise();
                resolve(answer);
            } catch (err) {
                reject(err);
            } finally {
                //loading.dismiss();
            }
        });
    }


    
    
}